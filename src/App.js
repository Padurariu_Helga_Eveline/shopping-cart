
import Cart from "./components/Cart";
import { Navbar } from "./components/Navbar";
import { HomeScreen } from "./screens/HomeScreen";
import { Store } from "./screens/Store";
import { Container } from "react-bootstrap";
import {Routes, Route} from "react-router-dom"




function App() {
  
  return (
     < >
      <Navbar />
      <Container>
        <Routes>
          <Route path="/"element ={<HomeScreen />} />
          <Route path="/store" element={<Store  />}/>
        </Routes>
      </Container>
      <Cart/>
     </>
  );
}

export default App;
