const products = [
  {
    id: 1,
    name: "AllProd",
    price: 1.20,
    imgUrl:
      " https://images.unsplash.com/photo-1608198093002-ad4e005484ec?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTF8fGJyZWFkfGVufDB8fDB8fA%3D%3D&auto=format&fit=crop&w=500&q=60 ",
  },
  {
    id: 2,
    name: "Bread",
    price: 2.10,
    imgUrl:
      "https://images.unsplash.com/photo-1509440159596-0249088772ff?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1172&q=80",
  },
  {
    id: 3,
    name: "Ciabattino",
    price: 1.10,
    imgUrl:
      "https://images.unsplash.com/photo-1511278475330-1a31a6fc4dcc?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTI4fHxicmVhZHxlbnwwfHwwfHw%3D&auto=format&fit=crop&w=500&q=60",
  },
  {
    id: 4,
    name: "Burger",
    price: 1.10,
    imgUrl:
      "https://media.istockphoto.com/photos/junk-food-homemade-beef-burgers-on-vintage-wooden-background-picture-id1302436326?b=1&k=20&m=1302436326&s=170667a&w=0&h=MvSjb8R4lOJT_NteIiZxfIoZvXIkKucRUVeViFBLVN4=",
  },
  {
    id: 5,
    name: "Cozonaci",
    price: 2.30,
    imgUrl:
      "https://images.unsplash.com/photo-1587232408234-53831c4efd7f?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTF8fHN3ZWV0JTIwYnJlYWR8ZW58MHx8MHx8&auto=format&fit=crop&w=500&q=60",
  },
  {
    id: 6,
    name: "Strudele",
    price: 3.30,
    imgUrl:
      "https://media.istockphoto.com/photos/whole-apple-strudel-picture-id116032671?b=1&k=20&m=116032671&s=170667a&w=0&h=z2C7Q40wfoIL5XmwYi6YfRxUI3cag9TWKoPBvRVd8r4=",
  },
];

export default products;
