import products from "../data";
import "./Store.css";
import ProductCard from "../components/ProductCard";

export function  Store () {
  return (
    <div className="products__wrapper">
      {products.map((product) => (
        <ProductCard key={product.id} product={product} />
      ))}
    </div>
  );
};
// export default Store;

// import Logo from "../screens/Logo.jpg"
// export  function Store ()  {
//         return(
//     <div>
//          <h1> My Store</h1>
//         < img src={Logo} alt="Logo"
//          variant="top"
//          height="650px"
//         style={{ objectFit: "cover" }}
//         />
//     </div>
// )

// }
