import Logo from "../screens/Logo.jpg";

export function HomeScreen() {
  return (
    <div style={{ textAlign: "center" }}><h1>All Products</h1>
      <img
        src={Logo}
        alt="Logo"
        variant="top"
        height="650px"
        style={{ objectFit: "cover" }}
      />
    </div>
  );
}

