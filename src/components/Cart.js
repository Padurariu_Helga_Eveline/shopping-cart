import "./Cart.css";
import { useContext } from "react";
import CartContext from "./context/cart/CartContext";
import CartItem from "./CartItem";
import { NumericFormat } from "react-number-format";
// import { Offcanvas, OffcanvasBody,Stack } from "react-bootstrap";

const Cart = () => {
  const { showCart, cartItems, showHideCart } = useContext(CartContext);

  return (
    <>
      {showCart && (
        <div className="cart__wrapper">
          <div style={{ textAlign: "right" }}>
            <i style={{ cursor: "pointer" }} aria-hidden="true"></i>
          </div>
          <div className="closeButton">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="18"
              height="18"
              fill="currentColor"
              className="bi bi-x-circle"
              viewBox="0 0 16 16"
              onClick={showHideCart}
            >
              <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
              <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z" />
            </svg>
          </div>
          <div className="cart__innerWrapper">
            {cartItems.length === 0 ? (
              <h4>Cart is Empty</h4>
            ) : (
              <ul>
                {cartItems.map((item) => (
                  <CartItem key={item.id} item={item} />
                ))}
              </ul>
            )}
          </div>
          <div className="Cart__cartTotal">
            <div>Cart Total </div>
            <div></div>
            <div style={{ marginLeft: 5 }}>
              <NumericFormat
                value={cartItems.reduce(
                  (amount, item) => item.price + amount,
                  0
                )}
                prefix={"$"}
              />
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default Cart;
