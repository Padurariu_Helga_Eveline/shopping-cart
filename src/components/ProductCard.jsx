
import "./ProductCard.css"
import CartContext from "./context/cart/CartContext";
import { useContext } from "react";


const ProductCard = ({ product}) => {
    const { addCartItem } = useContext(CartContext);
  return (
    <div className="productCard__wrapper "> 
      <div>
        <img className="productCard__img" src={product.imgUrl}   alt=""
          variant="top"
          height="650px"
          style={{ objectFit: "cover" }}/> 
          <span className="fs-2 d-flex align-items-center"> <h4> {product.name}</h4>
            </span>
          <div className="ProductCard__price ms-2 text-muted">
          <h4> ${product.price}</h4>

        </div>
        <button className="ProductCard__button" onClick={() => addCartItem(product)}>Add to cart</button>
      </div>
    </div>
  );
};
export default ProductCard;
