import "./CartItem.css";
import CartContext from "./context/cart/CartContext";
import { useContext } from "react";


const CartItem = ({item}) => {
    const { removeItem } = useContext(CartContext);
    return (
 
        <li className="CartItem__item">
                <img src={item.imgUrl} alt=""/>
                <div className=" mb-4">
                  {item.name}  ${item.price}
                </div>
                <button className="CartItem__button" 
                onClick={()=> removeItem(item.id)}>Remove
                </button>
        </li>
      

    );
};
export default CartItem;