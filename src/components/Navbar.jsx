import "./Navbar.css";
import {  Container, Nav, Navbar as NavbarBs } from "react-bootstrap";
import { useContext } from "react";
import CartContext from "./context/cart/CartContext";
import { NavLink } from "react-router-dom";

export function Navbar() {
  const { cartItems, showHideCart } = useContext(CartContext);

  return (
    <NavbarBs sticky="top" className=" nav  shadow-sm mb-4">
      <Container>
        <Nav className="me-auto">
        <Nav.Link to="/" as={NavLink}> Home</Nav.Link>
        <Nav.Link to="/store" as={NavLink}>Store</Nav.Link>
    
        </Nav>
        <div className="nav__left"></div>
        <div className="nav__middle">
          <div className="input__wrapper">
            <input type="text" />
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="16"
              height="16"
              fill="currentColor"
              class="bi bi-search"
              viewBox="0 0 16 16"
            >
              <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z" />
            </svg>
            <i className="fas fa-search" />
          </div>
        </div>
        <div className="nav__right">
          <div className="cart__icon " >
         

          <svg
              xmlns="http://www.w3.org/2000/svg"
              width="16"
              height="16"
              fill="currentColor"
              className="bi bi-cart"
              viewBox="0 0 16 16"
              aria-hidden="true"
              onClick={showHideCart}
            >
              <path d="M0 1.5A.5.5 0 0 1 .5 1H2a.5.5 0 0 1 .485.379L2.89 3H14.5a.5.5 0 0 1 .491.592l-1.5 8A.5.5 0 0 1 13 12H4a.5.5 0 0 1-.491-.408L2.01 3.607 1.61 2H.5a.5.5 0 0 1-.5-.5zM3.102 4l1.313 7h8.17l1.313-7H3.102zM5 12a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm7 0a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm-7 1a1 1 0 1 1 0 2 1 1 0 0 1 0-2zm7 0a1 1 0 1 1 0 2 1 1 0 0 1 0-2z" /> 
              
            </svg>

            {cartItems.length > 0 && (
              <div className="item__count ">
                <span>{cartItems.length}</span>
              </div>
            )}
         </div>
       </div>
       </Container>
    </NavbarBs>
  );   
 }         
       